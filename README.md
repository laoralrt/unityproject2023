Projet Unity 2022/2023
Laora LAURENT LP1B
projet réalisé individuellement (mais évidemment non sans entraide avec d'autres camarades)

Jeu réalisé : Flappy Bird
- Lancer le jeu sur la Scène 1 - Load
- Cliquer sur le bouton une fois sur la Scène 2 - Menu
- Pour faire sauter l'oiseau, appuyer sur la touche espace, faire un clic gauche de la souris ou toucher l'écran du téléphone
- Une fois sur la Scène 4 - Game Over, rappuyer sur l'écran, sur la touche espace ou faire un clic gauche de la souris pour revenir au menu principal

Le jeu a été développé sur un format 640*1136

/!\ Bien penser à mettre le son pour jouer, et ce dès la Scène 1 !

Fonctionnalités principales :
- les doubles flêches vertes permettent de multiplier la vitesse par une valeur random entre 1.1 et 1.5 (pour aller plus vite)
- les doubles flêches violettes permettet de multiplier la vitesse par une valeur random entre 0.5 et 0.9 (pour aller moins vite)
une fois une des ces doubles flêches entrées en collision avec l'oiseau, leur effet dure un temps random également, durant lequel aucune double flêche n'est visible, et une fois ce temps écoulé elles ne réapparaissent pas tout de suite (pour éviter d'avoir un comportement trop homogène)
- sauvegarde du score entre la scène 3 et la scène 4 pour l'affiche sur la scène Game Over

Toutes les difficultés rencontrées ont été résolues de la même manière : en me documentant
Et grâce à la documentation étoffée de Unity et des forums, je n'ai pas eu de difficultés qui ont persisté trop longtemps

J'avais cependant d'autres idées de fonctionnalités que je n'ai pas mises en oeuvre mais que je me serais vue faire :
- inverser la pesanteur (tomber vers le haut et sauter vers le bas) quand l'oiseau entre en collision avec un certain gameobject
- inverser la direction quand l'oiseau entre en collision avec un certain gameobject (voler vers la gauche et changer également les vitesses en x des pipes et des backgrounds par leur opposé)
- ajouter un classement de toutes les personnes ayant joué au jeu, et dans ce cas demander un pseudo à chaque début de partie, sauvegardé de la même manière que le score, avec un DontDestroyOnLoad
- faire des tuyaux qui bougent (comme si le jeu avait besoin d'être encore plus difficile et enrageant ^^)

Ressources ajoutées :
- Les sons ont été pris sur le site https://lasonotheque.org/ , une banque de sons gratuits et libres de droits
- Les doubles flêches ont été réalisées sur le site https://www.piskelapp.com/ 

J'espère en tout cas que tester ce jeu vous aura plu !