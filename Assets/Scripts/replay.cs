using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement ;

public class replay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("GameState").GetComponent<score>().ShowScore() ;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") || Input.touchCount>0 || Input.GetMouseButton(0)) // si appui touche espace ou appui clic gauche souris ou écran on retourne au menu
        {
            GameObject.Find("GameState").GetComponent<score>().ResetScore() ;
            Invoke("goScene2", 0f) ;
        }
    }

    void goScene2() {
        SceneManager.LoadScene("scene2-menu") ;
    }
}
