using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class score : MonoBehaviour
{

    public static score Instance ;
    public int scorePartie ;

    private void Awake() 
    {
        if (Instance != null)
        {
            Destroy(gameObject) ;
            return ;
        }

        Instance = this ;
        DontDestroyOnLoad(gameObject) ; // cela permet de conserver la valeur même en chargeant une nouvelle scène
    }

    public void AddScore()
    {
        score.Instance.scorePartie +=1 ;
        Debug.Log(score.Instance.scorePartie.ToString()) ;
        ShowScore() ;
    } 

    public void ShowScore()
    {
        GameObject.Find("Score").GetComponent<UnityEngine.UI.Text>().text = Instance.scorePartie.ToString() ; // on actualise l'affichage du texte du score
    }

    public void ResetScore()
    {
        score.Instance.scorePartie = 0 ;
    }
}
