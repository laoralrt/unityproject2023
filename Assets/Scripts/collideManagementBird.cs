using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour
{

    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 siz ;
    private bool colliderActivated ; // cet attribut ne sert qu'au moment où on perd, pour éviter de continuer de gagner des points
    private float coefSpeed ; // coefficient de vitesse quand on passe des doubles-flêches, attribut de la classe pour une utilisation globale dans plusieurs fonctions

    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,1,0));
        siz.x = GetComponent<SpriteRenderer>().bounds.size.x;
        siz.y = GetComponent<SpriteRenderer>().bounds.size.y;
        colliderActivated = true ;
        coefSpeed = 1f ;
    }

    // Update is called once per frame
    void Update()
    {
        // on teste à tout moment si l'oiseau sort de l'écran, si c'est le cas on perd
        if (transform.position.y < leftBottomCameraBorder.y - (siz.y/2) || transform.position.y > leftTopCameraBorder.y + (siz.y/2))
        {
            Debug.Log("sortie d'écran") ;
            FinDePartie() ;
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (colliderActivated) // true si on n'a pas encore perdu
        {
            if(collider.name == "upPipe" || collider.name == "downPipe")
            {
                Debug.Log("pipe touché");
                FinDePartie() ;
            }
            if (collider.name == "box1" || collider.name == "box2")
            {
                Debug.Log("vortex touché");
                GameObject.Find("GameState").GetComponent<score>().AddScore() ;
            }
            if (collider.name == "speed")
            {
                Debug.Log("accélération") ;
                coefSpeed = Random.Range(1.1f, 1.5f) ;
                changementVitesseGameObjects(coefSpeed, true) ; // on change donc la vitesse des pipesPair et des backgrounds (car ce n'est pas l'oiseau qui bouge)
                // désactiver les prochains speed
                GameObject.Find("speed").GetComponent<BoxCollider2D>().enabled = false  ;
                GameObject.Find("speed").GetComponent<Renderer>().enabled = false  ;
                GameObject.Find("speedFalse").GetComponent<BoxCollider2D>().enabled = false  ;
                GameObject.Find("speedFalse").GetComponent<Renderer>().enabled = false  ;
                // calcul d'une durée random du speed
                float duree = Random.Range(2f, 6f) ;
                Invoke("retourVitesseNormale", duree) ; 
            }
            if (collider.name == "speedFalse")
            {
                Debug.Log("décélération") ;
                coefSpeed = Random.Range(0.5f, 0.9f) ;
                changementVitesseGameObjects(coefSpeed, true) ;
                // désactiver les prochains speed
                GameObject.Find("speed").GetComponent<BoxCollider2D>().enabled = false  ;
                GameObject.Find("speed").GetComponent<Renderer>().enabled = false  ;
                GameObject.Find("speedFalse").GetComponent<BoxCollider2D>().enabled = false  ;
                GameObject.Find("speedFalse").GetComponent<Renderer>().enabled = false  ;
                // calcul d'une durée random du speed
                float duree = Random.Range(2f, 6f) ;
                Invoke("retourVitesseNormale", duree) ;
            }
        }
        else // dans le cas où on a perdu et on est en train de tomber en faisant une rotation
        {
            return ;
        }
    }

    void changementVitesseGameObjects(float coefAcceleration, bool acceleration) {
        float coef ;
        if (acceleration) 
        { 
            coef = coefAcceleration ;
        }
        else
        {
            coef = 1/coefAcceleration ; // car diviser par un nombre revient à multiplier par son inverse, si on veut revenir à la normale on doit diviser par le coef d'accélération
        }
        GameObject.Find("pipesPair1").GetComponent<movePipes>().movement.x *= coef ;
        GameObject.Find("pipesPair2").GetComponent<movePipes>().movement.x *= coef ;
        GameObject.Find("bk1").GetComponent<moveBK>().vitesse *= coef ;
        GameObject.Find("bk2").GetComponent<moveBK>().vitesse *= coef ;
        GameObject.Find("bk3").GetComponent<moveBK>().vitesse *= coef ;
    }

    void retourVitesseNormale() {
        changementVitesseGameObjects(coefSpeed, false) ;
        float duree = Random.Range(5f, 8f) ;
        Invoke("nouveauSpeed", duree) ;
    }

    void nouveauSpeed() { // fait réapparaître les flêches et réactive leur boxcollider
        GameObject.Find("speed").GetComponent<BoxCollider2D>().enabled = true  ;
        GameObject.Find("speed").GetComponent<Renderer>().enabled = true  ;
        GameObject.Find("speedFalse").GetComponent<BoxCollider2D>().enabled = true  ;
        GameObject.Find("speedFalse").GetComponent<Renderer>().enabled = true  ;
    }

    void FinDePartie() // si on perd (sortie d'écran ou collision avec tuyau)
    {
        colliderActivated = false ; // pour ne plus ajouter de points après avoir touché un pipe (si en tombant on passe un vortex la fonction OnTriggerEnter2D ne fera rien)
        GetComponent<endAction>().enabled = true;
        GetComponent<collideManagementBird>().enabled = false;
    }
}
