using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour
{

    public Vector2 movement ;
    public GameObject pipe1Up ;
    public GameObject pipe1Down ;
    public GameObject vortex ;
    public GameObject speed ;
    private Transform pipe1UpOriginalTransform ;
    private Transform pipe1DownOriginalTransform ;
    private Transform vortexOriginalTransform ;
    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 siz ;

    // Start is called before the first frame update
    void Start()
    {
        movement = new Vector2(-2f, 0) ; // on fixe la vitesse (vitesse en x négative car déplacement vers la gauche)
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,1,0));
        pipe1UpOriginalTransform = pipe1Up.transform ;
        pipe1DownOriginalTransform = pipe1Down.transform ;
        vortexOriginalTransform = vortex.transform ;
    }

    // Update is called once per frame
    void Update()
    {
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
        pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
        vortex.GetComponent<Rigidbody2D>().velocity = movement;
        speed.GetComponent<Rigidbody2D>().velocity = movement ; // on doit faire bouger les pipes, les vortex (invisibles, qui donnent les points) et les flêches
        siz.x = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.x; // Récuperation de la taille d’un pipe
        siz.y = pipe1Up.GetComponent<SpriteRenderer>().bounds.size.y; // Suffisant car ils ont la même taille
        // Le pipe est sorti de l’écran ? Si oui appel de la méthode moveToRightPipe
        if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
    }
    void moveToRightPipe()
    {
        float randomY = Random.Range (leftTopCameraBorder.y - (siz.y/10) - 6.5f, leftBottomCameraBorder.y + (siz.y/10) + 6.5f) ; // calcul de la position y du centre du vortex
        float posX = rightBottomCameraBorder.x + (siz.x / 2); // Calcul du X du bord droite de l’écran

        Vector3 tmpPos = new Vector3 (posX, randomY, vortex.transform.position.z);
        vortex.transform.position = tmpPos ;

        speed.transform.position = tmpPos ; // la flêche étant au centre du vortex, la position en y est la même

        // Création du vector3 contenant la nouvelle position
        tmpPos = new Vector3 (posX, randomY+6.5f, pipe1Up.transform.position.z);
        // Affectation de cette nouvelle position au transform du gameObject
        pipe1Up.transform.position = tmpPos;

        // Idem pour le second pipe
        tmpPos = new Vector3 (posX, randomY-6.5f, pipe1Down.transform.position.z);
        pipe1Down.transform.position = tmpPos;

        
    }
}
