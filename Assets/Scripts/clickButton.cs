using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement ;

public class clickButton : MonoBehaviour
{
    private AudioSource source ;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>() ; // c'est le petit son qu'on entend quand on appuie sur le bouton dans la scène 2
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onClick(){ // cette fonction est appelée quand on clique sur le bouton
        Debug.Log ("Clicked");
        playSource() ; // on joue alors le son
        Invoke("goScene3", 1f) ; // on attend 1 seconde avant de changer de scène à cause du délai avant que le son ne sorte
    }

    void playSource() {
        source.Play() ;
        return ;
    }


    void goScene3() {
        SceneManager.LoadScene("scene3-game") ;
    }
}
