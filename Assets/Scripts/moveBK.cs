using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBK : MonoBehaviour
{
    public float vitesse ;
    private Vector3 leftBottomCameraBorder;
    private Vector3 siz ;

    // Start is called before the first frame update
    void Start()
    {
        vitesse = -3f ;
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
    }
    
    // Update is called once per frame
    // The positionRestartX variable is public.
    // It is set with the transform.position.x background3 value.

    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(vitesse , 0) ; // on fixe la vitesse des backgrounds
        // If the backgound exits the screen
        // Set the X positon with the original backGround3 X position
        if (transform.position.x < leftBottomCameraBorder.x - (siz.x)) 
        {
            transform.position = new Vector3(12.3f ,transform.position.y,transform.position.z); 
            // le background fait 12.4 de largeur mais ici on utilise 12.3
            // de ce fait les backgrounds se superposent légèrement ce qui limite les espaces entre eux
        }

        
    }
    
}
