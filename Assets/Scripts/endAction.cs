using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement ;

public class endAction : MonoBehaviour
{

    /*
    endAction :
    • will destroy the touchAction script to avoid all user interactions
    • will rotate, in a loop, the bird
    • will test if the bird is exiting the screen area
    • will launch the End scene
    */

    private Vector3 leftBottomCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<touchAction>().enabled = false; // on ne peut plus intéragir avec l'oiseau étant donné qu'on a perdu
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,0,0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(0,1,0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3(1,1,0));
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 15, Space.Self); // l'oiseau tombe en faisant des rotatios
        if (transform.position.y < leftBottomCameraBorder.y) // une fois qu'on sort de l'écran en tombant on change de scène
        {
            // on met la page de fin
            Debug.Log("sortie d'écran, fin du jeu") ;
            Invoke("goScene4", 0f) ;
        }
    }

    void goScene4() {
        SceneManager.LoadScene("scene4-gameover") ;
    }
}
